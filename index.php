<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Filmovi</title>	
	<meta name="description" content="">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" media="screen"/>
	<link rel="stylesheet" href="css/flaticon/flaticon.css" media="screen"/>
	</head>
	
	<body>
		<div class="wrapper">
		
			<!--HEADER-->
			<?php include ('header.html') ?>
			<!--KRAJ HEADER-->
			
			<!--CONTAINER-->
			<div class="container">
							
				<!--REGISTRACIJA-->
				

 <?php 
 
        
        if(!isset($_GET["link"])) 
        $_GET["link"]="index"; 
        
        switch ($_GET["link"]) {   
            
            case "movies":  
            include("movies.php");  
            break;  
            
            case "genre":  
            include("genre.php");  
            break;  
            
            case "schedule":  
            include("schedule.php");  
            break; 
            
            default:  
            include("movies.php");  
            break; 

            } 
?>
				</div>	
				
				
			
				<!--FOOTER-->
			<?php include 'footer.html' ?>
				<!--KRAJ FOOTER-->
		</div>
	</body>
</html>