<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Update</title>	
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" media="screen"/>
	<link rel="stylesheet" href="css/flaticon/flaticon.css" media="screen"/>
	</head>
	
	<body>
		<div class="wrapper">
		
			<!--HEADER-->
			<?php include ('header.html') ?>
			<!--KRAJ HEADER-->
			
			<!--CONTAINER-->
			<div class="container">
							
				<!--REGISTRACIJA-->
				<div class="registracija">
					<div class="title">
					<h2>Izmena podataka</h2>		
					</div>	
					
<?php
        define("SECRET","@3eweHjdsdfuihjhjhj#VGVgggg!");
        require('db_config.php');
        
        if(isset($_GET["movieID"]))
        $movieID = $_GET["movieID"];
    
        $sql = "SELECT movies.id, movies.naziv, movies.trajanje, movies.opis, movies.id_zanr, genre.zanr
        FROM movies
        INNER JOIN genre ON movies.id_zanr = genre.id
        WHERE movies.id = $movieID";
		
        $result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
             
        if(mysqli_num_rows($result)>0)
        {
            while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC)) // MYSQLI_ASSOC, MYSQLI_BOTH,MYSQLI_NUM
            {     
            $id = $row["id"];
            $naziv = $row["naziv"];
            $id_zanr = $row["id_zanr"];
            $trajanje = $row["trajanje"];
            $opis = $row["opis"];
            }
           
            mysqli_free_result($result);
        }
        mysqli_close($connection);

?>		
					<form action="realUpdate.php" method="post">
						<div class="registracija-box">
                    
							<p>ID filma</p>
							<input type="number" name="id" min="1" max="20" value="<?php echo $id; ?>">
							
							<p>Naziv</p>
							<input type="text" name="naziv" value="<?php echo $naziv; ?>">
							
							<p>Žanr:<p>
							<input class="genre" type="number" name="id_zanr" min="1" max="5" value="<?php echo $id_zanr; ?>">
							
							<p>Trajanje:<p>
							<input type="text" name="trajanje" value="<?php echo $trajanje; ?>">
							
							<p>Opis</p>
							<textarea type="text" name="opis" value="<?php echo $opis; ?>"></textarea>
							
							<button type="submit" name="submit" value="Submit"> Pošalji</button>

						</div>
					</form>
				</div>	
			</div>
			
				<!--FOOTER-->
			<?php include ('footer.html') ?>
				<!--KRAJ FOOTER-->
		</div>
	</body>
</html>
    